# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PATH & colors
[ -f "$HOME/.profile" ] && source "$HOME/.profile"

# Command aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

export XDG_STATE_HOME="${HOME}/.local/state"
export HISTFILE="${XDG_STATE_HOME}/bash/history"


# Use Vi-like bindings
set -o vi

# Clear screen on Ctrl+l
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# Remove annoying bell sound on tty (thanks Lenovo)
bind 'set bell-style none'

# Shortcuts
aur=${HOME}/build/aur
abs=${HOME}/build/abs


# Default PS1
#PS1='[\u@\h \W]\$ '

# Custom PS1; refer to ~/Templates/colors.css &&
# https://jonasjacek.github.io/colors/ &&
# https://bashrcgenerator.com before editing
PS1='\[\033[1;38;5;217m\]｢\[\e[m\]\[\033[1;38;5;219m\]\u\[\e[m\]\[\033[1;38;5;15m\]@\[\e[m\]\[\033[1;38;5;159m\]\h\[\e[m\] \[\033[1;38;5;229m\]\W\[\e[m\]\[\033[38;5;217m\]｣\[\e[m\]\[\033[1;38;5;157m\]λ\[\e[m\] '


# pfetch
export PF_INFO="ascii title os kernel pkgs wm memory uptime"
export PF_COL1=6

setKeyboardLight () {
    dbus-send --system --type=method_call  --dest="org.freedesktop.UPower" "/org/freedesktop/UPower/KbdBacklight" "org.freedesktop.UPower.KbdBacklight.SetBrightness" int32:$1
}

ipact () {
    for ip in 192.168.1.{1..254}; do
        ping -c 1 -W 1 $ip | grep "64 bytes" &
    done
}

gitcfg () {
    git config user.name "$1" && git config user.email "$2" && echo "Changed default username & email address for this repo"
}
