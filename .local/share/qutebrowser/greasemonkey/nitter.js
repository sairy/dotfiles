// ==UserScript==
// @name           Nitter redirect
// @namespace      mira's userscripts
// @match          http://twitter.com/*
// @match          https://twitter.com/*
// @match          http://www.twitter.com/*
// @match          https://www.twitter.com/*
// @run-at         document-start
// ==/UserScript==

location.href=location.href.replace(/(www\.)?twitter\.com/, "nitter.pussthecat.org");
