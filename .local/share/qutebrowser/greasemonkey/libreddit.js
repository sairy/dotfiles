// ==UserScript==
// @name           Libreddit redirect
// @namespace      mira's userscripts
// @match          http://reddit.com/*
// @match          https://reddit.com/*
// @match          http://www.reddit.com/*
// @match          https://www.reddit.com/*
// @run-at         document-start
// ==/UserScript==

location.href=location.href.replace(/http(s)?:\/\/(www\.)?reddit\.com/, "http://0.0.0.0:8080");
