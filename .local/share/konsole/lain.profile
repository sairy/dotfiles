[Appearance]
BoldIntense=true
ColorScheme=Catppuccin
Font=Monaco,12,-1,5,50,0,0,0,0,0
TabColor=23,23,34,0
UseFontLineChararacters=true

[Cursor Options]
CursorShape=2

[General]
Command=/bin/zsh
DimWhenInactive=true
Directory=/home/mira
Icon=
InvertSelectionColors=true
LocalTabTitleFormat=%w : %n
Name=lain
Parent=FALLBACK/
StartInCurrentSessionDir=false

[Interaction Options]
MiddleClickPasteMode=0
TextEditorCmd=6
TextEditorCmdCustom=NVIMCFG=/home/mira/.config/nvim nvim PATH:LINE:COLUMN
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
TripleClickMode=1
UnderlineFilesEnabled=false
WordCharacters=:@-./_~?&=%+#

[Keyboard]
KeyBindings=default

[Scrolling]
HistorySize=500
ScrollBarPosition=2

[Terminal Features]
BellMode=3
BlinkingCursorEnabled=true
ReverseUrlHints=true
UrlHintsModifiers=67108864
