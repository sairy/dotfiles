[Appearance]
BoldIntense=true
ColorScheme=scratchy-mocha
Font=FiraCode Nerd Font,12,-1,5,53,0,0,0,0,0,Retina
IgnoreWcWidth=false
TabColor=23,23,34,0
UseFontLineChararacters=true
WordMode=true
WordModeAscii=false

[Cursor Options]
CursorShape=2

[Encoding Options]
DefaultEncoding=UTF-8

[General]
Command=/bin/zsh
DimWhenInactive=false
Directory=/home/mira
Icon=
InvertSelectionColors=true
LocalTabTitleFormat=%w : %n
Name=mira
Parent=FALLBACK/
StartInCurrentSessionDir=false

[Interaction Options]
MiddleClickPasteMode=0
TextEditorCmd=6
TextEditorCmdCustom=NVIMCFG=/home/mira/.config/nvim nvim PATH:LINE:COLUMN
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
TripleClickMode=0
UnderlineFilesEnabled=false
WordCharacters=:@-./_~?&=%+#

[Keyboard]
KeyBindings=default

[Scrolling]
HistorySize=500
ScrollBarPosition=2

[Terminal Features]
BellMode=3
BidiTableDirOverride=true
BlinkingCursorEnabled=true
ReverseUrlHints=true
UrlHintsModifiers=67108864
