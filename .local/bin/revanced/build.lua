#!/bin/lua

local g = {
    WORKDIR = os.getenv('WORKDIR'),
    DRY_RUN = false,
    CMD = nil,

    -- output file aliases
    APKNAMES = {
        youtube_ = 'yt',
        vanced = 'ytv',
        music = 'ytm',

        twitter = 'tw',
        musically = 'tt',
        trill = 'tt',
        spotify = 'sp',

        teslacoilsw = 'nova',
        nyx = 'nyx',
        iconpackstudio = 'ips',
        wallpapers = 'wp',
        ticktick = 'task',
        taskerm = 'taskm',

        citra = 'ctr',
        hexedit = 'hex',
        binarymode = 'bin',

        twitch = 'ttv',
        reddit = 'rd',

        warnapp = 'ww',
        windyapp = 'wa',
        -- ecmwf = 'ec',

        bmf2go = 'bmf',
        ['gv.oe'] = 'oe',
        myexpenses = 'myex',
        expensemanager = 'exmngr',
    }
}

g.CMD = [=[
java -jar __rdir__/cli/build/libs/revanced-cli-%s-all.jar \
-b __rdir__/patches/build/libs/revanced-patches-%s.jar \
-m __rdir__/integrations/app/build/outputs/apk/release/app-release-unsigned.apk \
-a %s \
-o %s \
-c \
-t /tmp/revanced]=]
-- swipe-controls | general-ads



local function help()
    print(
        string.format(
            'Usage:\n  %s APK_FILE [excludes | cli-args]...',
            arg[0]:gsub('.+/', ''):gsub('/', '')
        )
    )
    os.exit(0)
end

---@return versions
local function getvers()
    ---@class versions
    ---@field cli string
    ---@field patches string
    local t = {}

    local cmd = 'cd %s/%s && git describe --tags'

    for _, d in ipairs { 'cli', 'patches' } do
        local p = assert(io.popen(cmd:format(g.WORKDIR, d), 'r'), 'getvers: popen')

        ---@type string
        local v = p:read('*l')
        p:close()

        if not v then
            error('getvers: could not get version info for ' .. d)
        elseif v:match('^v') then
            v = v:sub(2, #v)
        end

        local dash = v:find('-')
        if type(dash) == 'number' then
            v = v:sub(1, dash-1)
        end

        t[d] = v
    end

    return t
end

---Finds which name to use for the output apk
--This is done so that the revanced cli can always find
--the keystore file, thus allowing the user to update
--the app without first uninstalling the previous version
---@param which string
---@return string
local function findapk(which)
    for expr, pfx in pairs(g.APKNAMES) do
        if which:match(expr) then
            return pfx .. '.apk'
        end
    end

    warn('@on')
    warn('findapk: unknown apk name. Using "revanced-filename" as output')
    warn('@off')
    return string.format('revanced-%s', which)
end

local function parseargs()
    -- faster string concat
    local t = { g.CMD }

    local i = 2
    while i <= #arg do
        local fmt = ' \\\n-e %s'

        if arg[i] == '--' then
            break
        elseif arg[i] == '-h' then
            help()
        elseif arg[i] == '--dry' then
            g.DRY_RUN = true
            goto continue
        elseif arg[i]:sub(1, 2) == '--' then
            fmt = ' \\\n%s'
        elseif arg[i]:sub(1, 1) == '-' then
            table.insert(t, string.format(' \\\n%s %s', arg[i], arg[i+1]))
            i = i + 1
            goto continue
        end

        table.insert(t, fmt:format(arg[i]))

        ::continue::
        i = i + 1
    end

    g.CMD = table.concat(t)
end

---@param outfile string
local function tweakcmd(outfile)
    if not outfile:match(g.APKNAMES.youtube_) and
        not outfile:match(g.APKNAMES.musically)
    then
        -- integrations only needed for youtube
        g.CMD = g.CMD:gsub('-m .+%.apk \\\n', '')
    end

    --[[
    if outfile:match(g.APKNAMES.musically) then
        -- https://github.com/revanced/revanced-patches/issues/340
        g.CMD = g.CMD .. ' \\\n-r'
    end
    --]]
end

--[[ main ]]--
do
    setmetatable(arg, {
        __newindex = function ()
            return error('immutable')
        end
    })

    if arg[1] == '-h' or arg[1] == '--help' then
        help()
    end
    parseargs()

    if not g.WORKDIR then
        local p = io.popen(string.format('dirname "%s"', arg[0]), 'r')
        if not p then
            g.WORKDIR = os.getenv('PWD') or '.'
        else
            g.WORKDIR = p:read('*l')
            p:close()
        end
    end

    g.CMD = g.CMD:gsub('__rdir__', g.WORKDIR)

    local apk = arg[1]
    local out = findapk(apk)
    local vers = getvers()

    tweakcmd(out)

    local cmd = g.CMD:format(vers.cli, vers.patches, apk, out)
    print('Command to run:')
    print(cmd)

    if g.DRY_RUN then
        return
    end

    if not os.execute(cmd) then
        error('main: error patching apk: ' .. apk)
    end
end

-- EOF
