#!/bin/sh

###################### User Settings ######################
#
## Path to the android SDK
sdk="${HOME}/.local/share/android-sdk"
#
## Gradle flags
gflags='--parallel'
#
## Depth (git repos)
# depth='--depth 1'
## Default revanced repos to update (in build order)
default_dirs='patcher patches integrations cli'
#
###########################################################

# PREFIX="$(realpath "$0" | xargs -rI {} dirname {})"
PREFIX="$(dirname "$0")"
CLONE_PFX="git@github.com:revanced/revanced"
export ANDROID_HOME="${sdk:-$ANDROID_HOME}"

# Gradle command
GRADLE="$(command -v gradle)"
[ -z "${GRADLE}" ] && GRADLE='./gradlew'

e() {
    col="$1"
    sym="$2"
    shift 2

    printf '\033[1;38;5;%sm%s\033[0;1m %s\n\033[0m' "$col" "$sym" "$@"
    unset col sym
}

msg() {
    e 5 "===>" "$@"
}

smsg() {
    e 2 "==>" "$@"
}

err() {
    e 1 "==>" "$@"
}

lsmsg() {
    e 4 "=>" "$@"
}

killjava() {
    msg "Killing dormant jvm processes"
    pidof -q java && killall java
}

clone() {
    smsg "'$1' not found. Cloning..."
    git clone ${depth} "${CLONE_PFX}-${1}" "${PREFIX}/${1}" || true
}

get_sha() {
    git rev-parse HEAD || true
}

update_or_clone() {
    msg "Updating revanced repos"

    for i in "${PREFIX}/"$dirs; do
        echo
        base="$(basename "$i")"

        if [ -d "$i" ]; then
            cd "$i" || continue
        else
            clone "${base}"
            build="${build} ${base}"
            continue
        fi

        sha="$(get_sha)"

        smsg "Updating ${base}..."
        git pull ${depth} || {
            err "Failed to pull ${base}"
            cd ..
            continue
        }

        new_sha="$(get_sha)"

        [ "${sha}" != "${new_sha}" ] && build="${build} ${base}"
        unset sha new_sha base

        cd ..
    done
}

build() {
    echo
    build="$(echo "$build" | sed 's/^\s*//; s/\s*$//' | tr -s ' ')"

    msg "Using '${GRADLE}' as the gradle script"
    echo

    msg "Projects to build:"
    for i in $build; do
        lsmsg "$i"
    done
    echo

    # Kill dormant jvm processes from gradle's daemon on exit
    trap 'killjava' TERM EXIT INT

    for i in "${PREFIX}/"$build; do
        cd "$i" || continue
        base="$(basename "$i")"

        smsg "Building ${base}..."
        "${GRADLE}" $gflags build || err "Build failed for ${base}"
        unset base

        cd ..
        echo
    done
}

###### main ######

[ "$1" = "-s" ] && {
    unset gflags
    shift
}

if [ $# -gt 0 ]; then
    dirs="$*"
    shift $#
else
    dirs="${default_dirs}"
fi

update_or_clone
[ "${build}" ] && build


# EOF
