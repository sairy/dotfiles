#!/bin/sh

pname="${0##*/}"

usage() {
    cat <<-EOF
	Usage:
	  ${pname} [ -hy ] [ -c cover ] TRACK...

	Options:
	  -h            Display this message
	  -y            Replaces each TRACK with the corresponding new file
	  -c cover      Image to use as cover

	Dependencies:
	  ffmpeg

	EOF
}

die() {
    printf '%s: %s\n' "${pname}" "$@" 1>&2
    echo
    usage
    exit 1
}

deptest() {
    for i in "$@"; do
        command -v "$i" >/dev/null 2>&1 || die "command $i nor found"
    done
}

embed() {
    cnt=1
    ttl=$#

    for file in "$@"; do
        echo "Embeding cover to file(s)... (${cnt}/${ttl})"
        echo

        ffmpeg -loglevel quiet \
            -i "${file}" -i "${cover}" -f "${file##*.}" \
            -map 0:a -map 1 -codec copy -metadata:s:v title='Album cover' \
            -metadata:s:v comment='Cover (front)' -disposition:v attached_pic \
            "/tmp/${file}" # in /tmp because ffmpeg is sensitive to file exts

        if [ -n "${replace}" ]; then
            mv -f "/tmp/${file}" "${PWD}/${file}"
        else
            mv -f "/tmp/${file}" "${PWD}/${file}.emb"
        fi

        cnt=$(( cnt + 1 ))
    done
    unset file cnt ttl
}

#### main ####

optstr=":hyc:"

deptest ffmpeg

while getopts "${optstr}" flag; do
    case ${flag} in
        h)
            usage
            exit 0
            ;;
        y)
            replace=1
            ;;
        c)
            cover="${OPTARG}"
            ;;
        \?)
            die "option -${OPTARG} not found"
            ;;
        :)
            die "option -${OPTARG} requires an argument"
            ;;
    esac
done

shift $(( OPTIND -1 ))

[ -z "${cover}" ] && die "no cover chosen"

[ $# -lt 1 ] && die "no files chosen"

embed "$@"


# EOF
