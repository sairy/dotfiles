#!/bin/sh

# from /etc/profile
append_path() {
    if [ "$1" = "-f" ]; then
        prepend=1
        shift
    fi
    p="$1"

    case ":${PATH}:" in
        *:"${p}":*)
            # dir already in path; do nothing
            ;;
        *)
            if [ "${prepend}" ]; then
                PATH="${p}${PATH:+:$PATH}"
            else
                PATH="${PATH:+$PATH:}${p}"
            fi
            ;;
    esac

    unset p prepend
}

append_path -f "${HOME}/.local/bin/npm"
append_path -f "${HOME}/.local/bin"

append_path "${HOME}/.local/bin/lua"
append_path "${HOME}/.local/bin/haskell"
append_path "${HOME}/.local/bin/haskell/cabal-bin"
append_path "${HOME}/.local/bin/go"
append_path "${HOME}/.local/bin/python"
# append_path "${HOME}/.local/bin/python"

export PATH
unset -f append_path

# java
export JAVA_HOME="/usr/lib/jvm/default"
export JDTLS_HOME="${XDG_DATA_HOME}/jdtls"


# pfetch
# info can be:
# ascii, title, os, kernel, uptime, pkgs, memory, shell, editor, wm, de, palete
# export PF_INFO="ascii title os kernel pkgs wm shell memory"
# export PF_ALIGN="10"


# ls Colors
#di=1;38;2;182;175;255
#di=1;38;2;176;115;255

export LS_COLORS="di=1;38;2;245;194;231:\
ln=1;38;2;85;255;255:\
ow=1;38;2;182;175;255:\
ex=1;38;5;202:\
\
*.bak=38;5;244:\
*.txt=38;5;199:\
*.md=1;4;38;5;197:\
*.diff=38;5;30:\
*.patch=38;5;30:\
\
*.pdf=38;5;33:\
*.epub=38;5;33:\
*.odt=38;5;38:\
*.doc=38;5;38:\
*.docx=38;5;38:\
\
*.jpg=38;5;219:\
*.jpeg=38;5;219:\
*.gif=38;5;219:\
*.png=38;5;177:\
*.webp=38;5;177:\
\
*.flac=38;5;49:\
*.mp3=38;5;49:\
*.ogg=38;5;49:\
*.opus=38;5;49:\
\
*.wav=38;5;49:\
*.mkv=38;5;81:\
*.mp4=38;5;81:\
*.mpeg=38;5;219:\
*.webm=38;5;81:\
*.wmv=38;5;81:\
*.avi=38;5;81:\
\
*.sig=38;5;164:\
*.tar=38;5;76:\
*.arc=38;5;76:\
*.zip=38;5;76:\
*.Z=38;5;76:\
*.gz=38;5;76:\
*.lz=38;5;76:\
*.lz4=38;5;76:\
*.lzma=38;5;76:\
*.xz=38;5;76:\
*.zst=38;5;76:\
*.bz2=38;5;76:\
*.jar=38;5;76:\
*.rar=38;5;76:\
*.7z=38;5;76:\
\
*.c=38;2;81;91;242:\
*.h=38;2;242;234;119:\
*.vim=38;2;1;151;51:\
*.lua=38;2;109;145;202:\
*.hs=38;2;203;166;247:\
*.lhs=38;2;203;166;247:\
*.java=38;5;130:\
*.html=38;2;249;136;68:\
*.css=38;2;18;158;217:\
*.ts=38;2;81;154;186:\
*.js=38;2;234;213;78:\
*.svelte=38;2;255;62;0:\
\
*Makefile=1;4;38;2;120;212;237:\
*GNUmakefile=1;4;38;2;120;212;237:\
*makefile=1;4;38;2;120;212;237:\
*.mk=1;4;38;2;120;212;237:\
*LICENSE=1;4;38;2;120;212;237:\
*COPYING=1;4;38;2;120;212;237:\
*PKGBUILD=4;38;5;51:\
*.deb=38;5;197:\
*README=1;4;38;5;197:\
*TODO=1;4;38;5;197:\
"

# fzf colors
export FZF_DEFAULT_OPTS="\
--color=bg+:#313244,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#f5e0dc,fg+:#f5c2e7,prompt:#cba6f7,hl+:#f38ba8"

# export FZF_DEFAULT_OPTS='--color=bg+:#302D41,spinner:#F8BD96,hl:#F28FAD --color=fg:#D9E0EE,header:#F28FAD,info:#DDB6F2,pointer:#F8BD96 --color=marker:#F8BD96,fg+:#F2CDCD,prompt:#DDB6F2,hl+:#F28FAD'

# export FZF_DEFAULT_OPTS="--color=\
# hl:#c2ff73:bold,\
# bg+:#06030f,\
# fg+:#b073ff:bold,\
# gutter:#06030f,\
# hl+:#c2ff73,\
# query:#c2ff73:regular,\
# prompt:#b6afff,\
# pointer:#b073ff,\
# info:#d2738a,\
# spinner:#d2738a\
# "
