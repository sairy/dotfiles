#!/bin/zsh

# KDE Dolphin MIME handling outside Plasma
# see: https://bbs.archlinux.org/viewtopic.php?id=295236
export XDG_MENU_PREFIX="plasma-"

# XDG Base Directories
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"

export BROWSER="firefox"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"
export MANSECT="1:1p:n:l:8:2:3:3type:3p:0:0p:5:4:9:6:7"

# Clean-up
export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc"
export KDEHOME="${XDG_CONFIG_HOME}/kde"
export PIPX_BIN_DIR="${HOME}/.local/bin/python"
export PIPX_HOME="${HOME}/.local/lib/pipx"
export RIPGREP_CONFIG_PATH="${XDG_CONFIG_HOME}/ripgrep/rgrc"
export TERMINFO="${XDG_DATA_HOME}/terminfo"
export TERMINFO_DIRS="${TERMINFO}:/usr/share/terminfo"
export WGETRC="${XDG_CONFIG_HOME}/wgetrc"
export XCURSOR_PATH="/usr/share/icons:${XDG_DATA_HOME}/icons"

## Languages
export ANDROID_HOME="${XDG_DATA_HOME}/android"
export CARGO_HOME="${HOME}/.local/lib/cargo"
export DOTNET_CLI_TELEMETRY_OPTOUT="1"
export GHCUP_INSTALL_BASE_PREFIX="${HOME}/.local/lib"
# export GHCUP_USE_XDG_DIRS="true"
export GOPATH="${XDG_DATA_HOME}/go"
export GRADLE_USER_HOME="${XDG_DATA_HOME}/gradle"
export M2_HOME="${XDG_DATA_HOME}/m2"
export NODE_REPL_HISTORY="${XDG_CACHE_HOME}/node/repl_history"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/init.py"
export ROSWELL_HOME="${HOME}/.local/lib/roswell"
export RUSTUP_HOME="${HOME}/.local/lib/rustup"
export SBCL_HOME="/usr/lib/sbcl"
export STACK_ROOT="${XDG_DATA_HOME}/stack"
export TEXMFHOME="${XDG_DATA_HOME}/texmf"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=${XDG_CONFIG_HOME}/java"

# Maria DB
export MYSQL_HISTFILE="${XDG_DATA_HOME}/mysql_history"

# Shortcuts
export abs="${HOME}/build/abs"
export aur="${HOME}/build/aur"
export dwm="${HOME}/dev/gitlab/dwm"
export swm="${HOME}/.config/stumpwm"

export ZDOTDIR="${HOME}/.config/zsh"
export NVIMCFG="${HOME}/.config/nvim"
export PLUGDIR="/usr/share/zsh/plugins"

# Fix conflict with dwm
export _JAVA_AWT_WM_NONREPARENTING=1

# nix
export NIX_PATH="${XDG_DATA_HOME}/nix/defexpr/channels"
export NIX_PROFILES="${XDG_DATA_HOME}/nix/profile"
# export XDG_DATA_DIRS="${HOME}/.nix-profile/share:${XDG_DATA_DIRS}"

# Enable hwdec for mpv
export LIBVA_DRIVER_NAME=radeonsi
