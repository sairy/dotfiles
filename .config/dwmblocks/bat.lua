#!/bin/lua

WHICH = 'rem'

local icons = {
    empty = ' ',
    half = ' ',
    almost = ' ',
    full = '',
    charger = '󰢝',
}

---@class battery
---@field name string
---@field path function

---@class batteries
---@field pfx string
---@field battery battery
---@field charger battery

local function bats_init()
    local bat_pfx = '/sys/class/power_supply/'
    local bat_path = function (self)
        return bat_pfx .. self.name .. '/'
    end

    ---@type batteries
    return {
        pfx = bat_pfx,
        battery = {
            name = 'BAT0',
            path = bat_path,
        },
        charger = {
            name = 'ADP0',
            path = bat_path,
        }
    }
end

local bats = bats_init()

local checkwhich = function ()
    local file = 'online'

    local f = assert(io.open(bats.charger:path() .. file, 'r'))
    local charging = f:read('*l') == '1'
    f:close()

    return charging and bats.charger or bats.battery
end

local read_level = function ()
    local f = assert(io.open(bats.battery:path() .. 'capacity', 'r'))
    local level = f:read('*l')
    f:close()

    return assert(tonumber(level), [[battery percentage was not a number]])
end

local choose_icon = function (bat, level)
    local icon

    if bat == bats.battery then
        if level < 11 then
            icon = icons.empty
        elseif level < 75 then
            icon = icons.half
        elseif level < 98 then
            icon = icons.almost
        else
            icon = icons.full
        end
    else
        icon = icons.charger
    end

    return icon
end

local bat_perc = function (bat)
    local level = read_level()
    local icon = choose_icon(bat, level)

    return string.format('%s %d%%', icon, level)
end

local bat_rem = function (bat)
    local pick_and_read = function (f1, f2)
        local f, value

        f = io.open(bat:path() .. f1, 'r')
            or assert(io.open(bat:path() .. f2, 'r'))

        value = f:read('*n')
        f:close()

        return assert(value)
    end

    if bat ~= bats.battery then
        return icons.charger
    end

    local charge = pick_and_read('energy_now', 'charge_now')
    local current = pick_and_read('power_now', 'current_now')

    local time = charge / current -- I = dQ/dt

    local hr, min = math.modf(time)
    min = math.floor(min * 60)

    local icon = choose_icon(bat, read_level())

    return string.format("%s %dh %dm", icon, hr, min)
end

--[[ main ]]--

local bat = checkwhich()

local func = (bat == bats.charger or WHICH == 'perc') and bat_perc or bat_rem

print(func(bat))

-- EOF
