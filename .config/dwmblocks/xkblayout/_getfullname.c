#include <stdio.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XKBstr.h>


__attribute__((__noreturn__))
static void die(const char *restrict msg);

void
die(const char *restrict msg)
{
    fputs(msg, stderr);
    exit(EXIT_FAILURE);
}

int
main(void)
{
    Display *dpy = XOpenDisplay(NULL);
    if (!dpy)
        die("could not open Display");


    XkbStateRec state;
    XkbGetState(dpy, XkbUseCoreKbd, &state);


    XkbDescRec *desc = XkbGetMap(dpy, XkbNamesMask, XkbUseCoreKbd);
    if (!desc)
        die("could not get map");


    XkbGetNames(dpy, XkbGroupNamesMask, desc);
    if (!desc->names)
        die("no groups :(");


    Atom sym = desc->names->groups[state.group];

    char *layout = XGetAtomName(dpy, sym);

    if (!layout)
        die("could not ge map");

    printf("layout: %s\n", layout);


    /* cleanup */
    XFree(layout);
    XkbFreeKeyboard(desc, XkbNamesMask, True);
    XCloseDisplay(dpy);

    return EXIT_SUCCESS;
}
