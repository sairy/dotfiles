#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>

#include "util.h"

void
die(const char *restrict fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);


    if (*fmt && fmt[strlen(fmt)-1] == ' ')
        perror(NULL);
    else
        fputc('\n', stderr);

    exit(EXIT_FAILURE);
}

char *
layout_from_vd(Display *dpy, const XkbRF_VarDefsRec *vd)
{
    XkbStateRec state;
    XkbGetState(dpy, XkbUseCoreKbd, &state);

    char *layout = strtok(vd->layout, ",");
    for (unsigned char i = 0; i < state.group; i++) {
        if (!(layout = strtok(NULL, ",")))
            return NULL;
    }

    return layout;
}
