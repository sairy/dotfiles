#pragma once

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <X11/XKBlib.h>
#include <X11/extensions/XKBrules.h>

__attribute__((__noreturn__, __format__(printf, 1, 2)))
void die(const char *restrict fmt, ...);

char *layout_from_vd(Display *dpy, const XkbRF_VarDefsRec *vd);
