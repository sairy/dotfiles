#include <stdio.h>
#include <stdlib.h>

/* Xlib */
#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XKBrules.h>

#include "util.h"

#define ICON    ("")

int
main(void)
{
    Display *dpy;

    if (!(dpy = XOpenDisplay(NULL)))
        die("could not open display: ");

    XkbRF_VarDefsRec vd;
    XkbRF_GetNamesProp(dpy, NULL, &vd);

    printf("%s %s\n", ICON, vd.layout);

    XCloseDisplay(dpy);
    return EXIT_SUCCESS;
}
