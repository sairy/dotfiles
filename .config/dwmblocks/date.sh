#!/bin/sh

ICON="󰃭"
FMT="+%m月%d日 (%a) %H:%M"

printf "%s %s" "${ICON}" "$(date "${FMT}")"
