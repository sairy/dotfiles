#!/bin/sh

ICON=''
layout="$(setxkbmap -query | awk '/layout/ { print gensub(/,.*/, "", "g", $2) }')"

printf '%s %s' "${ICON}" "${layout}"
