#!/bin/lua

local icons = {
    muted = ' ',
    quiet = ' ',
    loud = ' '
}

local mic_str = '[ muted]'

local audio = {
    comm = 'pamixer ',
    step = '2',
    -- sink = 'alsa_output.pci-0000_03_00.6.analog-stereo',
    -- src  = 'alsa_input.pci-0000_03_00.6.analog-stereo',

    sink = '@DEFAULT_SINK@',
    src = '@DEFAULT_SOURCE@',
}

---@param src string
---@return boolean
local is_muted = function (src)
    local f = assert(io.popen(audio.comm .. src .. ' --get-mute', 'r'))
    local muted = f:read('*l')
    f:close()

    return assert(muted) == 'true'
end

---@param src string
---@return integer
local read_vol = function (src)
    local f = assert(io.popen(audio.comm .. src .. ' --get-volume', 'r'))
    local vol = f:read('*n')
    f:close()

    return assert(vol)
end


local update = function ()
    local mic = is_muted('--source ' .. audio.src) and (' ' .. mic_str) or ''

    if is_muted('--sink ' .. audio.sink) then
        return string.format('%s%s%s', icons.muted, 'muted', mic)
    end

    local vol = read_vol('--sink ' .. audio.sink)
    local icon

    if vol == 0 then
        icon = icons.muted
    elseif vol < 50 then
        icon = icons.quiet
    else
        icon = icons.loud
    end

    return string.format('%s%2d%%%s', icon, vol, mic)
end

print(update())
