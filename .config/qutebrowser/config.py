import catppuccin

config.load_autoconfig()

catppuccin.setup(c, 'mocha')
# catppuccin.tampilan(c)

config.bind('<Alt-Shift-u>', 'spawn --userscript qute-keepassxc --key B2B41A4B42A0D0918F9AB46F71CA3900DAD1237B', mode='insert')
config.bind('pw', 'spawn --userscript qute-keepassxc --key B2B41A4B42A0D0918F9AB46F71CA3900DAD1237B', mode='normal')
