import os
import sys

import readline
import atexit

try:
    import colored_traceback.auto
except ImportError:
    pass


# readline.parse_and_bind("set editing-mode vi")

#
print() # separate ps1 from header
sys.ps1 = '\033[1;38;5;228m' + 'py' + '\033[0m' + \
          '\033[1;38;5;105m' + 'shell' + '\033[1;38;5;15m' '> ' + \
          '\033[0m'



def write_hist():
    try:
        readline.write_history_file(history)
    except OSError:
        pass

if 'XDG_CACHE_HOME' in os.environ:
    history = os.path.join(
        os.path.expanduser(os.environ['XDG_CACHE_HOME']),
        'python', 'history.py')
else:
    history = os.path.join(
        os.path.expanduser('~'),
        '.cache', 'python', 'history.py')

history = os.path.abspath(history)
_dir, _ = os.path.split(history)
os.makedirs(_dir, exist_ok=True)

try:
    readline.read_history_file(history)
except OSError:
    pass

atexit.register(write_hist)
