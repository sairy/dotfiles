#!/bin/zsh

# Change cursor shape for different vi modes.
# from: https://github.com/LukeSmithxyz/voidrice/blob/master/.config/zsh/.zshrc

zle-keymap-select() {
    case $KEYMAP in
        vicmd)
            echo -ne '\e[2 q' # block (static)
            ;;
        viins|main)
            echo -ne '\e[3 q' # underline (blinking)
            ;;
    esac
}

zle -N zle-keymap-select

zle-line-init() {
    # initiate `vi insert` as keymap
    # (can be removed if `bindkey -V` has been set elsewhere)
    zle -K viins
    echo -ne "\e[3 q"
}

zle -N zle-line-init

# Use underline cursor for each new prompt.
_zsh_underline_preexec() { echo -ne '\e[3 q' }
add-zsh-hook preexec _zsh_underline_preexec
# preexec_functions+=(_zsh_underline_preexec)
