#!/bin/zsh

_zsh_config_dirs=(${ZDOTDIR} ${PLUGDIR} ${HOME} ${XDG_CONFIG_HOME})

load() {

    for dir in ${_zsh_config_dirs[@]}; do
        if [ -f "${dir}/${1}" ]; then
            source "${dir}/${1}"
            return
        elif [ -f "${dir}/${1%%.*}/${1}.zsh" ]; then
            # for pacman installed plugins
            source "${dir}/${1%%.*}/${1}.zsh"
            return
        fi
    done

    print -P 'Failed to load %U${1}%u' 1>&2
}

# vim: ft=sh
