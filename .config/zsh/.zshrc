#!/bin/zsh

HISTFILE="${XDG_CACHE_HOME}/zsh/history"
HISTSIZE=1000
SAVEHIST=1000
# setopt hist_expire_dups_first
# setopt hist_ignore_dups
setopt hist_ignore_space
# setopt hist_verify

# add timestamps to history
setopt EXTENDED_HISTORY

# cd to a directory without typing in 'cd'
setopt autocd nomatch

# Disable beeping on errors
unsetopt beep

# Comments in shell
setopt interactivecomments

# Sets vi input (-e for emacs)
bindkey -v
export KEYTIMEOUT=1

bindkey ' ' magic-space

_zle_woman () { command woman; }
zle -N _zle_woman
bindkey '^o' '_zle_woman'
# bindkey -s '^e' '^uwoman -v zathura\n'

autoload -z edit-command-line
zle -N edit-command-line
bindkey '^p' edit-command-line


# Auto add '' when pasting URLs
autoload -Uz bracketed-paste-url-magic &&
    zle -N bracketed-paste bracketed-paste-url-magic
autoload -Uz url-quote-magic &&
    zle -N self-insert url-quote-magic

# Add custom funcs to the precmd and preexec arrays
# Same thing as precmd_funtions+=(<function>)
# or preexec+=(<function>)
autoload -Uz add-zsh-hook

# Set the window title
_zsh_title_precmd() {
    local term="$(pidname "${PPID}")"

    case "${term}" in
        konsole)
            printf "\033]0;%s@%s:%s\007" \
                "${USER}" "${HOST}" "${PWD/#$HOME/~}"
            return 0
            ;;
        st)
            ;;
        *)
            term="$(capitalize "${term}")"
            ;;
    esac

    local shell="$(pidname "$$")"
    printf "\033]0;%s@%s:%s : %s — %s\007" \
        "${USER}" "${HOST}" "${PWD/#$HOME/~}" \
        "${shell}" "${term}"
}

add-zsh-hook precmd _zsh_title_precmd

# function to load every other config file
if [ -f "${ZDOTDIR}/load.zsh" ]; then
    source "${ZDOTDIR}/load.zsh"
else
    print -P "Failed to source %U${ZDOTDIR}/load.zsh%u"
    return 1
fi

load ".profile"
load "cursor.zsh"

if command -v /usr/bin/starship >/dev/null 2>&1; then
    eval "$(/usr/bin/starship init zsh)"
else
    # Programs to exclude from showing how long they took
    export ZSH_TIMER_EXCLUDES=(ark bat btop fv his htop less \
        lf lua man more mpv nsxiv nvim \
        py python rifle sbcl scrot sxiv \
        top woman zathura)

    # If programs take less than this to run
    # the time they took won't be shown
    export ZSH_TIMER_MINIMUM=3

    load "PS1.zsh"
    load "timer.zsh"
fi


load "completion.zsh"
load "functions.zsh"

load "aliasrc"

load "zsh-autosuggestions"
load "fast-syntax-highlighting.plugin"

# vim: ft=sh
