#!/bin/zsh

unset brl user at host cwd git brr prompt

function load-minimal()
{
    host='%F{blue}󰊠 %f'
    cwd='%F{green}%1~%f'
    git='%F{211}${vcs_info_msg_0_}%f' #131?
    prompt=' %F{yellow}%f'
}

function load-tty()
{
    user='%B%F{magenta}%n%f'
    at='%F{white}@%f'
    host='%F{cyan}%m%f'
    cwd=' %F{yellow}%1~%f'
    git='%F{red}${vcs_info_msg_0_}%f'
    prompt=' %F{green}%%%f%b'
}

function load-full()
{
    brl='%B%F{217}｢%f'
    user='%F{219}%n%f'
    at='%F{15}@%f'
    host='%F{159}%m%f'
    cwd=' %F{229}%1~%f'
    git='%F{203}${vcs_info_msg_0_}%f'
    brr='%F{217}｣%f'
    prompt='%F{157}𝝺%f%b'
}

autoload -Uz promptinit && promptinit

# PS1
autoload -U colors && colors

case "${TERM}" in
    "st-256color"|"tmux-256color")
        load-minimal
        ;;
    "xterm-kitty")
        load-full
        # load-minimal
        # host='%F{blue}%f%K{blue}%F{16} 󰊠 %f%k%F{blue}%f '
        ;;
    "linux")
        load-tty
        ;;
    "xterm-256color")
        [ "$(pidname "${PPID}")" = "nvim" ] &&
            load-minimal || load-full
        ;;
    *)
        load-full
        ;;
esac

export PS1="${brl}${user}${at}${host}${cwd}${git}${brr}${prompt} "
# PS1="$host$cwd$git$prompt"
# PROMPT="%{$fg[blue]%}󰊠 %{$fg[green]%}%~ %{$vcs_info_msg_0_%}%{$fg[yellow]%} % %{$reset_color%}"


# Display git info in PS1
autoload -Uz vcs_info
setopt prompt_subst
add-zsh-hook precmd vcs_info
zstyle ':vcs_info:*' 	 check-for-changes true

zstyle ':vcs_info:*' 	 unstagedstr   ' *'
zstyle ':vcs_info:*' 	 stagedstr     ' +'
zstyle ':vcs_info:git:*' formats       ' (%b%u%c)'
zstyle ':vcs_info:git:*' actionformats ' (%b|%a%u%c)'

unset -f load-full load-minimal

# vim: ft=sh
