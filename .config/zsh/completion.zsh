#!/bin/zsh

zmodload -i zsh/complist
zstyle :compinstall filename "${ZDOTDIR}/.zshrc"
fpath+="${ZDOTDIR}/zfunc"
autoload -Uz compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' complete-options true

# Prevent autocompleting urls
zstyle ':completion:*:open:argument*' tag-order - '! urls'

# Add LS_Colors
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

compinit -d ${XDG_CACHE_HOME}/zsh/zcompdump-${ZSH_VERSION}

# complete dotfiles
_comp_options+=(globdots)


# Navigate autocomplete menu w/ vimkeys
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

## Auto Suggestions

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=245"

# Stategies used, in order
# history = most recent match
# completion = based on [TAB] completion
# match_prev_cmd = last command
ZSH_AUTOSUGGEST_STRATEGY=(completion history)

# Avoid completing strings that are too long
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=16

# Don't suggest if pattern matches
ZSH_AUTOSUGGEST_HISTORY_IGNORE="mpv *|git *|dots*"
