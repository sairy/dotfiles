#!/bin/zsh

# function die() {
#     printf "%s\n" $@ 1>&2
#     return 1
# }

function dd() {
    echo "You probably didn't want to call 'dd'"
    echo "You're welcome"
}

0() { true;  }
1() { false; }

function test_cmd() {
    command -v "$1" >/dev/null 2>&1
}

function test_undercurl() {
    printf '\033[4:3;58:2:1mCurly\033[0m\n'
}

# function cd() {
#     local last="${@: -1}"
#     local l1="${last:0:1}"

#     [ "${last}" ] && [ "${l1}" != "-" ] && [ "${l1}" != "+" ] &&
#         [ ! -d "${last}" ] && [ -w "$(dirname "${last}")" ] &&
#         mkdir -pv "${last}"

#     builtin cd "$@"
# }

function health() {
    full="$(cat /sys/class/power_supply/BAT0/energy_full)"
    full_des="$(cat /sys/class/power_supply/BAT0/energy_full_design)"

    echo $(( full * 100 / full_des ))
}

function battery() {
    head -n1 /sys/class/power_supply/BAT0/capacity
}

function screen() {
    xdpyinfo | awk '/dimensions/ { print $2 }'
}

# Too long to type
function gb() {
    cd "$OLDPWD"
}

# Change user name & commit email for a specific repo
function gitcfg() {
    [ "$1" = "-h" ] && {
        echo "Usage: gitcfg <username> <email>"
        return 0
    }
    git config user.name "$1" &&
        git config user.email "$2" &&
        print -P "Updated %U%Busername%b%u & %B%Uemail address%u%b for %Bthis%b repo"
}

# Go to the root of a git repo
function gr() {
    local root="$(git rev-parse --show-toplevel 2>/dev/null)"

    [ -z "${root}" ] || cd "${root}"
}

function ropen() {
    [ ! -d ".git" ] && { gr || return 1; }

    awk '/url = .+/ { print $3 }' .git/config |
        xargs -rI {} setsid -f xdg-open {}
    gb
}

# yt playlist
function muzika() {
    mpv --no-video --force-window=no \
        'https://youtube.com/playlist?list=PLZf5TfADZAlEsC6Cmmut_pAKa9LPFbZDK'
}

# Reload zsh config
function zsrc() {
    source "${ZDOTDIR}/.zshrc" &&
        print -P "%BUpdated zsh config%b" ||
        print -P "%BUpdate %B%Ufailed%u!%b" 1>&2
}


# execute a command from cmd history
function his() {
    local cmd="$(history | fzf --tac --no-sort | sed 's/^\s*[0-9]*\s*//')"
    zsh -c "${cmd}"
}
zle -N his
bindkey '^k' 'his'

function jsonfmt() {
    if [ $# -ne 2 ]; then
        cat <<-EOF
		Usage:
		  ${0##*/} inputfile outputfile
		EOF
        return 1
    fi

    python -m json.tool "$1" "$2"
}

function Xeph() {
    local scr="800x600"
    local dpy="1"
    Xephyr -br -ac -noreset -screen "${2:-$scr}" :${1:-$dpy}
}

function mvi() {
    ! test_cmd mvn && {
        echo "maven not installed" >&2
        return 1
    }

    local gid artid archetype

    archetype="maven-archetype-quickstart"

    while getopts ":g:a:" arg; do
        case "$arg" in
        g)
            gid="$OPTARG"
            ;;
        a)
            artid="$OPTARG"
            ;;
        esac
    done
    shift $((OPTIND - 1))

    if [ -z "$gid" ]; then
        printf "group id:\n> "
        read gid
    fi

    if [ -z "$artid" ]; then
        printf "artifact id:\n> "
        read artid
    fi

    # gen the project
    mvn -gs "${XDG_CONFIG_HOME}/maven/settings.xml" archetype:generate \
        -DgroupId="$gid" \
        -DartifactId="$artid" \
        -DarchetypeArtifactId="$archetype" \
        -DinteractiveMode=false

    cd "$artid"

    # get pom
    cp -f "$HOME/.config/java/pom.xml" .

    sed -i "s/vencimentos\.MainVencimentos/${gid}.App/; \
            s/vencimentos/$gid/; \
            s/earnings/$artid/" \
        "./pom.xml"

    # get mvnw
    mvn -gs "${XDG_CONFIG_HOME}/maven/settings.xml" -N io.takari:maven:wrapper
}

function jacoco() {
    gr &&
        mvn -gs "${XDG_CONFIG_HOME}/maven/settings.xml" \
            jacoco:prepare-agent \
            test \
            jacoco:report &&
        gb
}

# https://wiki.archlinux.org/title/Xrandr#Adding_undetected_resolutions
function 2nd_mon() {
    local mon w h

    case "$#" in
        1)
            mon="${1}"
            ;;
        3)
            w="$1"
            h="$2"
            mon="$3"
            ;;
    esac

    w="${w:-1920}"
    h="${h:-1080}"
    mon="${mon:-HDMI-A-0}"

    local modeline="$(cvt "$w" "$h" | tail -n1 | cut -d ' ' -f2-)"
    local profile="$(cut -d ' ' -f1 <<< "${modeline}")"

    # quotes not added on purpose
    xrandr --newmode $modeline
    xrandr --addmode "${mon}" "${profile}"
    xrandr --output "${mon}" --mode "${profile}"
}

function __2nd_mon() {
    xrandr --newmode "1920x1080_50.00"  141.50  1920 2032 2232 2544  1080 1083 1088 1114 -hsync +vsync
    xrandr --addmode HDMI-A-0 "1920x1080_50.00"
    xrandr --output HDMI-A-0 --mode "1920x1080_50.00"

    # xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
    # xrandr --addmode HDMI-A-0 1920x1080_60.00
    # xrandr --output HDMI-A-0 --mode 1920x1080_60.00

    xrandr --output eDP --primary --output HDMI-A-0 --right-of eDP

    # xwallpaper --output eDP --zoom "${HOME}/.config/dwm/wallpaper" \
    #            --output HDMI-A-0 --zoom "${HOME}/pics/flatppuccin_4k_macchiato.png"
}

# vim: ft=sh
