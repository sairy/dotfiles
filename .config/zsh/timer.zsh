#!/bin/zsh

# runs before command is actually executed
_zsh_timer_preexec() {
    # Grab command that will be run
    _command="${1%% *}"

    if [ "$_command" = "doas" ] || echo "$_command" | grep -q '='; then
        _command="$(echo "$1" | awk '{ print $1,$2 }')"
    fi

    local exclude
    for exclude in "${ZSH_TIMER_EXCLUDES[@]}"; do
        if [ "${_command}" = "${exclude}" ]; then
            # Don't show how long command took if
            # it's in the excludes array
            unset _command _timer
            return
        fi
    done

    # Set timer to time elapsed since shell invocation
    _timer=${SECONDS}
}

# runs before the next prompt is drawn
_zsh_timer_precmd() {
    if [ "${_timer}" ] && [ "${_command}" ]; then
        # Time since shell invocation minus $_timer variable
        _elapsed=$(( SECONDS - _timer ))

        # commands quicker than $ZSH_TIMER_MINIMUM
        # shouldn't show how long it took
        [ ${_elapsed} -ge ${ZSH_TIMER_MINIMUM} ] &&
            print -P "\n%B%F{228}> ${_command}:%f%b" \
                "%B%F{231}took ${_elapsed} seconds%f%b"
    fi

    unset _command _timer
}

add-zsh-hook preexec _zsh_timer_preexec
add-zsh-hook precmd  _zsh_timer_precmd


# vim: ft=sh
