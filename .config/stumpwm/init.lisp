;;;; vim:foldmethod=marker

(in-package :stumpwm)
(load "~/.local/lib/quicklisp/setup.lisp")
(push #p"/usr/share/common-lisp/systems/" asdf:*central-registry*)
;; (push #p"~/.local/lib/quicklisp/systems/" asdf:*central-registry*)
(push (merge-pathnames ".local/lib/quicklisp/systems/"
                       (user-homedir-pathname))
      asdf:*central-registry*)

(defvar *stump-home* "~/.config/stumpwm/")

(setf *startup-message* "^5*Welcome to ^Bmira's^b config of ^BStumpWM^b!
Hit ^6* Super+? ^5* to get help.")

;; fix cursor if using `startx`
(run-shell-command "xsetroot -cursor_name left_ptr")

;; (set-module-dir (concat *stump-home* "modules"))

(run-shell-command (concat *stump-home* "scripts/autostart"))

;; Load bindings right away to avoid being stuck with none on errors
(load (concat *stump-home* "bindings.lisp"))

(load (concat *stump-home* "audio.lisp"))

;; Change focus on mouse hover
(setf *mouse-focus-policy* :sloppy)

;; Input bar
(load (concat *stump-home* "msgbar.lisp"))

;; Windows
(load (concat *stump-home* "windows.lisp"))

(load (concat *stump-home* "mods.lisp"))

;;; EOF
