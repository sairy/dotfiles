(in-package :stumpwm)

(defcommand vol-change (action value)
    ((:string "Action ([i]nc/[d]ec): ")
     (:number "Value: "))
  (if (or (equal action "i") (equal action "d"))
      (run-shell-command (concat "pamixer -" action (write-to-string value)))
      (message "Unknown action: ~a" action)))

; FIX
(defcommand vol-mute (&optional arg)
    ((:number))
  (run-shell-command "pamixer -t"))
