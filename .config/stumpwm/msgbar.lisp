(in-package :stumpwm)

;; Colors
(set-fg-color "#9460d6")
(set-bg-color "#07030f")
(set-border-color "#b073ff")
(set-msg-border-width 2)

;; Positioning
(setf
  *message-window-gravity* :center
  *message-window-input-gravity* *message-window-gravity*
  *input-window-gravity* :top
  *message-window-y-padding* 5)
