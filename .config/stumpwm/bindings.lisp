(in-package :stumpwm)

;; Menu gets bound to mod4 with the autostart script
(set-prefix-key (kbd "Menu"))

(defvar *terminal* "konsole --profile lain")
(defvar *browser* "firefox")
(defvar *menu* "/usr/bin/dmenu_run -p \"Launch: \"")

(defmacro clear-binds (map &rest keys)
  "Remove @keys from @map"
  `(progn ,@(mapcar (lambda (key)
                      (list 'undefine-key map (kbd key)))
                    keys)))

#|
(defmacro add-binds (map &rest keybinds)
  "Same as #'fill-keymap but it doesn't reset the map"
  `(loop for ,i = ,keybinds then ,(cddr i)
         while i
         do (define-key ,map ,(first i) ,(second i))))


(add-binds *root-map*
           (kbd "M-a") "exec brave")
|#

; Removing default bindings {{{
(clear-binds *root-map* "a" "C-a" "b" "C-b" "c" "C-c" "e" "C-e" "C-g" "m" "C-m"
                        "F1" "F2" "F3" "F4" "F5" "F6" "F7" "F8" "F9" "F10")

(clear-binds *group-root-map* "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "o" "#" "F11")

;; (clear-binds *title-group-root-map* "o" "r" "R" "Q" "TAB" "M-TAB" "Up" "Down" "Left" "Right" "M-Up" "M-Down" "M-Left" "M-Right")
;; }}}

; Keymaps {{{
(define-key *root-map* (kbd "C-q") "restart-hard") ; reload stumpwm
(define-key *root-map* (kbd "Q") "quit")
(define-key *root-map* (kbd "q") "delete")
(define-key *root-map* (kbd "x") "remove")

(define-key *root-map* (kbd "ESC") "abort")

(define-key *root-map* (kbd "z") "lastmsg")
(define-key *root-map* (kbd "m") "fullscreen")

(define-key *root-map* (kbd "quoteleft") "exec") ; run shell command

(define-key *root-map* (kbd "S-Return") (concat "exec " *terminal*))
(define-key *root-map* (kbd "o") (concat "exec " *browser*))
(define-key *root-map* (kbd "O") "exec tor-browser")
(define-key *root-map* (kbd "p") (concat "exec " *menu*))

(define-key *root-map* (kbd "b") "mode-line")

(define-key *top-map* (kbd "XF86AudioMute") "vol-mute")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "vol-change i 2")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "vol-change d 2")
; }}}

; Navigating inside a workspace {{{
(define-key *root-map* (kbd "h") "move-focus left")
(define-key *root-map* (kbd "j") "move-focus down")
(define-key *root-map* (kbd "k") "move-focus up")
(define-key *root-map* (kbd "l") "move-focus right")

(define-key *top-map* (kbd "M-TAB") "fother")
; }}}

; Moving windows around {{{
(define-key *root-map* (kbd "H") "move-window left")
(define-key *root-map* (kbd "J") "move-window down")
(define-key *root-map* (kbd "K") "move-window up")
(define-key *root-map* (kbd "L") "move-window right")
; }}}

; WorkSpace definitions {{{
(defvar *ws-names* (list "alpha" "delta" "zeta" "lambda" "nu" "ksi" "sigma" "phi" "omega"))
; (defvar *ws-names* (list "α" "δ" "ζ" "λ" "ν" "ξ" "π" "φ" "ω"))

(grename (nth 0 *ws-names*)) ; rename default workspace to 0th item in *ws-names*

(dolist (ws (cdr *ws-names*)) ; create remaining workspaces
  (gnewbg ws))

(mapcar (lambda (n)
          (define-key *root-map*
                      (kbd (format nil "~a" n))
                      (format nil "gselect ~a" n))
          (let ((mv-to-sw '("!" "@" "#" "$" "%" "^" "&" "*" "(")))
            (define-key *root-map*
                        (kbd (nth (- n 1) mv-to-sw))
                        (format nil "gmove ~a" n))))
        '(1 2 3 4 5 6 7 9))

(define-key *root-map* (kbd "TAB") "gother")

; }}}

;;;; vim:foldmethod=marker
