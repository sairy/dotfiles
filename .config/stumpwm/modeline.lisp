(in-package :stumpwm)


; (defvar *mode-line-position* :top)

; (defvar *mode-line-pad-x* 10)
; (defvar *mode-line-pad-y* 10)

(setf
  *mode-line-background-color* "#06030f"
  *mode-line-border-color* *mode-line-background-color*
  *mode-line-foreground-color* "#b073ff")

(setf
  *group-format* " %t "
  *window-format* "^(:fg \"#bd93f9\")%n%s%c%m"
  *time-modeline-string* "%b %e (%a) %H:%M"
  *screen-mode-line-format*
      (list "%g %W^>%l | ^(:font 0) %B | ^(:font 1)%d"))

#| (setf
  *screen-mode-line-format*
      (list "%g %W^>|  %B | "
            '(:eval (run-shell-command "date '+%x (%a) %H:%M' | sed 's/.....//'" t)))) |#

(enable-mode-line (current-screen) (current-head) t)
