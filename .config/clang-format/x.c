/*
 * Exemple file of how the .clang-format configuration
 * will indent this C source file
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "my_header.h"
#include "zzz.h"

#define ARR_SIZE   2000
#define N_CHILDREN 10
#define LEN        (ARR_SIZE / N_CHILDREN)
#define BAD_IDX    255

int find_first_n(int n, const int *arr, size_t len);
int fun(void);

int
fun(void)
{
    static const char *restrict x; // hello
    int k;                         // there

    int j, i = 4;        // general
    float f = (float) i; // kenobi

    if (i > 3) {
        j = 14;
        i++;
    } else {
        j = 1;
    }

    switch (i) {
    case 1: {
        j = 1;
    }
    case 2: {
        f = f * 2;
    }
    }

    return 0;
}

int
find_first_n(int n, const int *arr, size_t len)
{
    size_t i;
    for (i = 0; i < len; i++)
        if (arr[i] == n)
            return i;
    return BAD_IDX;
}

int
main(void)
{
    size_t i, code;
    int n, status, offset;
    int arr[ARR_SIZE];
    pid_t p, pids[N_CHILDREN] = { 0 };

    srand(time(NULL) * getpid());
    for (i = 0; i < ARR_SIZE; i++)
        arr[i] = rand();
    n = rand();

    for (i = offset = 0; i < N_CHILDREN; i++, offset += 200) {
        switch ((p = fork())) {
        case -1:
            perror("fork: ");
            exit(EXIT_FAILURE);
        case 0: /* child */
            _exit(find_first_n(n, arr + offset, LEN));
        default: /* parent */
            pids[i] = p;
        }
    }

    fputs("Valid indexes: ", stderr);
    for (i = 0; i < N_CHILDREN; i++) {
        waitpid(pids[i], &status, 0);
        if (WIFEXITED(status) && (code = WEXITSTATUS(status)) != BAD_IDX)
            printf("%lu ", code + LEN * i);
    }
    putchar('\n');

    return EXIT_SUCCESS;
}
