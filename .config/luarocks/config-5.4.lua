---@diagnostic disable: lowercase-global
---@diagnostic disable: undefined-global

home_tree = home .. '/.local/lib/luarocks'

rocks_dir = home_tree .. 'lib/luarocks/rocks-5.4'

rocks_trees = {
    { name = 'user', root = home_tree },
    { name = 'system', root = '/usr' },
}

web_browser = os_getenv('BROWSER') or 'firefox'

-- deploy_bin_dir = rt_dir .. 'bin/luarocks'
-- deploy_lua_dir = rt_dir .. 'share/lua/5.4'
-- deploy_lib_dir = rt_dir .. 'lib/lua/5.4'
