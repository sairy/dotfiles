

;;; TODO: suppress the quickload output
;; (ql:quickload 'which)


;;; Variables ;;;

(setf
  *repl-name* "mira's REPL"
  *prompt* "sbcl> "
  *prompt2* "...>"
  *ret* "==> "
  #| *pygmentize* (which:which "idk kev ¯\_(ツ)_/¯")
  *pygmentize-options* "" |#
  *hist-file* "~/.cache/sbcli/history.lisp")
